exports.config = {
  specs: [
    'tests/**/*.spec.js'
  ],
  baseUrl: 'http://127.0.0.1:8080',
  capabilities: {
    'browserName': 'chrome'
  },
};

//import AngularJS
require('angular');
require('./scss/main.scss');
var acceuilContent = require('./controller/admin/accueil.html');
var accueilIndexContent = require('./controller/accueilIndex/accueilIndex.html');
var loginTemplate = require('./controller/authentification.html');
var layoutTemplate = require('./controller/admin/layout/layout.html');
var publicLayoutTemplate = require('./controller/layout/layout.html');
var inscriptionContent = require('./controller/inscription.html');

angular
    .module('blog', ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('adminLayout', {
                url: '/admin',
                template: layoutTemplate,
                abstract: true
            })
            .state('accueil', {
                parent: 'adminLayout',
                url: '',
                template: acceuilContent,
                controller: 'accueilCtrl',
            })
            .state('authentification', {
                url: '/authentification',
                template: loginTemplate,
                controller: 'authentificationCtrl',
            })
            .state('publicLayout', {
                template: publicLayoutTemplate,
                abstract: true,
            })
            .state('index', {
                template: accueilIndexContent,
                url: '/',
                parent: 'publicLayout',
                controller: 'accueilIndexCtrl',
            })
            .state('inscription', {
                template: inscriptionContent,
                url: '/inscription',
                parent: 'publicLayout',
                controller: 'inscriptionCtrl',
            });
        $urlRouterProvider.otherwise('/authentification');
    });

angular
    .module('blog')
    .controller('authentificationCtrl', ['$scope', '$state', function($scope, $state) {

        var whereToGo = 3;

        /*
         * il faut appeler un repository directement par le Nom
         * sinon on va devoir faire appel a l'ensemble des utilisateurs
         */

        /*
              var listUtilisateur = makeRequest('GET' ,'/utilisateurs');
         */

        var listUtilisateur = [{
                "pseudo": "naivo",
                "password": "naivo",
                "type": 0
            },
            {
                "pseudo": "nathanael",
                "password": "nath",
                "type": 1
            }
        ];

        /*
         *verification du login
         */

        $scope.verificationLogin = function() {
            console.log($scope.pseudo);
            console.log($scope.password);
            for (var i = 0; i < listUtilisateur.length; i++) {
                if (listUtilisateur[i].pseudo == $scope.pseudo && listUtilisateur[i].password == $scope.password) {
                    if (listUtilisateur[i].type == 1) {
                        whereToGo = 1;
                    } else if (listUtilisateur[i].type === 0) {
                        whereToGo = 0;
                    }
                } else {
                    console.log("error dans la comparaison");
                }
            }

        };

        /*
         * renvoi vers la bonne page soit user soit admin
         */

        $scope.checkCondition = function() {
            $scope.verificationLogin();
            console.log(whereToGo);
            if (whereToGo === 0) {
                console.log("bienvenu dans user");
                $state.go('index');
            } else if (whereToGo == 1) {
                console.log("bienvenu dans admin");
                $state.go('accueil');
            } else {
                console.log("bienvenu dans l'erreur");
            }
        };
    }]);

angular
    .module('blog')
    .controller('inscriptionCtrl', ['$scope', '$state', function($scope, $state) {

        $scope.documentsUser = [];

        /*
         * fonction de la creation d'un utilisateur
         * a envoyer a l'admin pour la creation dans la base de données
         */

        $scope.submitForm = function() {
            var nouvelUtilisateur = {
                "nom": $scope.Nom,
                "prenom": $scope.Prenom,
                "adresse": $scope.Adresse,
                "codePostal": $scope.CodePostal,
                "email": $scope.Email,
                "documents": $scope.documentsUser
            };

            console.log(nouvelUtilisateur);
            $state.go('index');

        };
    }]);

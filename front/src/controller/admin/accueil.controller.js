angular
    .module('blog')
    .controller('accueilCtrl', ['$scope', function($scope) {

        /*
         * affichage des objet non valider
         */

        $scope.validation = false;

        /*
         * test d'un tableau d'utilisateur recu
         */

        $scope.userList = [];
        for (var i = 0; i < 10; i++) {
            var nouvelUtilisateur = {
                "nom": "nomNivo " + i,
                "prenom": "prenomNivo " + i,
                "adresse": i + " alle de Paris",
                "codePostal": "7500" + i,
                "email": "toto" + i + "@mail.com",
                "valid": false
            };
            $scope.userList.push(nouvelUtilisateur);
        }

        /*
         * recuperation de l'envoi cote user pour nouvel utilisateur
         * envoi au back pour creation d'un nouvel utilisateur
         */

        $scope.creationUtilisateur = function(user) {
            console.log(user);
            user.valid = true;
        };

        /*
         * validation de l'ensemble des utilisateurs
         */

        $scope.terminerValidation = function(test) {
            console.log(test);
            $scope.validation = true;
        };

        /*
         * test liste de questions
         */

        $scope.questions = [{
                "text": "2 x 2?",
                "email": "toto1@mail.com",
                "done": false
            },
            {
                "text": "8 x 2?",
                "email": "toto2@mail.com",
                "done": false
            }
        ];

        /*
         *repondre aux question poser par l'utilisateur
         *en envoyant la reponse au mail lié
         */

        $scope.repondreQuestion = function(reponseQuestion, question) {
            console.log(reponseQuestion);
            console.log(question.email);
            question.done = true;
        };

    }]);

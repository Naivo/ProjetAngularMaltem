angular
    .module('blog')
    .controller('accueilIndexCtrl', ['$scope', function($scope) {

        /*
         * listes des action possible a effectuer + Documents User + affAsset
         */

        $scope.documentsUser = [];

        $scope.affAsset = true;

        $scope.assetsList = [{
                "action": "1er action a pouvoir etre realiser",
                "valeur": false,
                "id": 0
            },
            {
                "action": "2eme action a pouvoir etre realiser",
                "valeur": false,
                "id": 1
            },
            {
                "action": "3eme actions a pouvoir etre realiser",
                "valeur": false,
                "id": 2
            }
        ];
        console.log($scope.assetsList);
        /*
         * changer le statu d'une asset si valider
         */

        $scope.answers = [];

        $scope.stateChanged = function(assetId) {
            console.log("toto");
            console.log($scope.answers[assetId]);

            if ($scope.assetsList[assetId]) {
                $scope.assetsList[assetId].valeur = true;
            }
        };

        /*
         * Liste des actions valider par l'utilisateur a envoyer a l'admin avec liste pour validation
         */


        $scope.submitAsset = function() {
            console.log($scope.assetsList);
            for (var i = 0; i < $scope.assetsList.length; i++) {
                if ($scope.assetsList[i].valeur === true) {
                    console.log($scope.assetsList[i].action);
                }
            }

            $scope.affAsset = false;
        };

        /*
         * fonction l'upload d'un document
         * envoyer dans un objet document au controleur de l'admin
         */

        $scope.submitFormTwo = function() {
            var path = document.getElementById('fichier').value;

            $scope.documentsUser.push(path);
            console.log($scope.documentsUser);
        };

        /*
         * fonction d'envoi des questions : doit contenir la qustion et l'email de l'user pour repondre a l'admin
         * envoyer dans un objet question au controleur de l'admin pour la gestion des questions
         */

        $scope.envoiQuestion = function() {
            console.log($scope.question);
            console.log($scope.Email);
        };

        /*
         * envoi information about me
         */

         $scope.envoiAboutMe = function() {
             console.log($scope.aboutMe);
         };
    }]);

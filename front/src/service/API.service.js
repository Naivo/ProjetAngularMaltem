angular
    .module('blog')
    .factory('apiService', function($http) {
        var apiService = {};
        var baseUrl = '/api';
        apiService.makeRequest = function(method, url, data) {
            var config = {
                method: method,
                url: baseUrl + url
            };
            if (data && method === 'GET') {
                config.params = data;
            } else if (data) {
                config.data = data;
            }
            return $http(config).then(function(response) {
                return response.data;
            });
        };

        return apiService;
    });

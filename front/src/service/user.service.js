/**
 * Service de gestion de l'utilisateur
 */
angular
  .module('blog')
  .factory('userService', function() {
    var user = {};
    var userService = {};

    /**
     * Récupère l'utilisateur en cours
     * @return {} Utilisateur
     */
    userService.getUser = function() {
      return user;
    };

    /**
     * Retourne TRUE si l'utilisateur est authentifié
     * @return {bool}
     */
    userService.isUserAuthenticated = function(){
      return user && user.authenticated;
    };
    return userService;
  });

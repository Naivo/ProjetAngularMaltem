//import AngularJS
require('angular');
require('angular-ui-router');
require('angular-mocks');

//Import Bootstrap
require('./scss/main.scss');

//Import des modules
require('./blog.module');

//Import des contrôleurs
require('./controller/accueilIndex/accueilIndex.controller');
require('./controller/admin/accueil.controller');
require('./controller/authentification.controller');
require('./controller/inscription.controller');

//Import des services
require('./service/API.service.js');
require('./service/user.service.js');

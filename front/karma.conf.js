var webpackConfig = require('./webpack.config.js');
webpackConfig.devtool = 'inline-source-map';
module.exports = function(config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['jasmine'],
    reporters: ['progress', 'coverage'],
    files: [
      'src/main.js',
      'src/controller/**/*.controller.js',
      'src/**/*.service.js',
      'src/**/*.spec.js'
    ],
    preprocessors: {
      // add webpack as preprocessor
      'src/main.js': ['webpack'],
      'src/**/*.controller.js': ['coverage'],
      'src/**/*.service.js': ['coverage'],
    },

    // optionally, configure the reporter
    coverageReporter: {
      type: 'html',
      dir: 'dist/coverage/'
    },
    webpack: webpackConfig,
    phantomjsLauncher: {
      exitOnResourceError: true
    }
  });
};

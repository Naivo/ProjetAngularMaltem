var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var extractSass = new ExtractTextPlugin({
  filename: "style.css"
});

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.bundle.js'
  },
  module: {
    rules: [{
        test: /\.scss$/,
        use: extractSass.extract({
          use: [{
            loader: "css-loader"
          }, {
            loader: "sass-loader"
          }]
        })
      }, {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.(ttf|svg|woff2?|eot)$/,
        use: 'url-loader?limit=100000'
      }
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, "dist"),
    inline: true, //Pas d'utilisation de iframe,
    proxy: {
      "/api": {
        target: 'http://127.0.0.1/web/app_dev.php',
      }
    }
  },
  plugins: [
    extractSass,
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ]
};

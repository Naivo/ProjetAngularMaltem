var loadMock = function(data) {
    browser.addMockModule('httpMocker', function(data) {
        angular.module('httpMocker', ['ngMockE2E'])
            .run(function($httpBackend) {
                $httpBackend.whenGET('/api/articles').respond(data);
            });
    });
};

describe("La page d'accueil", function() {

    it("doit contenir une barre de navigateur", function() {
        browser.get('/');
        var navBar = element(by.css('nav'));
        expect(navBar.element(by.css('.navbar-brand')).getText()).toBe('Accueil');
        var navBarList = navBar.all(by.css('.navbar-nav li'));

        expect(navBarList.count()).toBe(2);

        var menuLinkAccueil = navBarList.first();
        expect(menuLinkAccueil.getText()).toBe('Accueil');
        expect(menuLinkAccueil.getAttribute('class')).toContain('active');

        var menuLinkList = navBarList.get(1);
        expect(menuLinkList.getText()).toBe('Liste des articles');
        expect(menuLinkList.getAttribute('class')).not.toContain('active');
        menuLinkList.click();

        expect(browser.getCurrentUrl()).toContain('#!/publicArticleList');
    });

    it("doit contenir un jumbotron", function() {
        browser.addMockModule('httpMocker', function() {
            angular.module('httpMocker', ['ngMockE2E'])
                .run(function($httpBackend) {
                    $httpBackend.whenGET('/api/articles').respond([{
                        "titre": "titre 3",
                        "contenu": "contenu 3"
                    }]);
                });
        });
        browser.get('/');
        var jumbotron = $('.jumbotron');
        expect(jumbotron.isDisplayed()).toEqual(true);
        expect(jumbotron.$('h1').getText()).toBe('titre 3');
        expect(jumbotron.$$('p').first().getText()).toBe('contenu 3');
    });

    it("doit afficher 2 articles sous le jumbotron", function() {
        browser.addMockModule('httpMocker', function() {
            angular.module('httpMocker', ['ngMockE2E'])
                .run(function($httpBackend) {
                    $httpBackend.whenGET('/api/articles').respond([{
                        "titre": "Mon titre",
                        "contenu": "Mon contenu"
                    }, {
                        "titre": "titre 2",
                        "contenu": "contenu 2"
                    }, {
                        "titre": "titre 3",
                        "contenu": "contenu 3"
                    }]);
                });
        });
        browser.get('/');
        var articlesUnderJumbotron = $$('.row>div').filter(function(element, index) {
            return element.isDisplayed();
        });
        expect(articlesUnderJumbotron.count()).toBe(2);
    });

    it("ne doit pas afficher plus de 4 article sur page d'acceuil", function() {

        browser.addMockModule('httpMocker', function() {
            angular.module('httpMocker', ['ngMockE2E'])
                .run(function($httpBackend) {
                    var listeArticle = [];
                    for (var i = 0; i < 10; i++) {
                        listeArticle.push({
                            "titre": "Mon titre " + i,
                            "contenu": "Mon contenu " + i
                        });
                    }
                    $httpBackend.whenGET('/api/articles').respond(listeArticle);
                });
        });
        browser.get('/');
        var articlesUnderJumbotron = $$('.row>div').filter(function(element, index) {
            return element.isDisplayed();
        });
        expect(articlesUnderJumbotron.count()).toBe(3);
    });
});

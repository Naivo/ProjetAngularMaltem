describe("la page publicArticleList ", function() {

    it("doit afficher un message si il n y a pas d'articles", function() {

        browser.addMockModule('httpMocker', function() {
            angular.module('httpMocker', ['ngMockE2E'])
                .run(function($httpBackend) {
                    $httpBackend.whenGET(/^\/api\/articles\?/).respond([]);
                });
        });

        browser.get("/");
        browser.setLocation("/publicArticleList");
        var messageVide = element(by.css("h2")).getText();
        expect(messageVide).toEqual("La liste est vide");


    });

    it("doit afficher une liste d'articles correspondant on nombre d'article envoyé", function() {

        browser.addMockModule('httpMocker', function() {
            angular.module('httpMocker', ['ngMockE2E'])
                .run(function($httpBackend) {
                    var listeArticle = [];
                    for (var i = 0; i < 10; i++) {
                        listeArticle.push({
                            "titre": "Mon titre " + i,
                            "contenu": "Mon contenu " + i
                        });
                    }
                    $httpBackend.whenGET(/^\/api\/articles\?/).respond(listeArticle);
                });
        });

        browser.get("/");
        browser.setLocation("/publicArticleList");

        var listeArticle = $$(".article");
        expect(listeArticle.count()).toBe(10);


    });

});

describe("la page d'article details ", function() {

  it("doit afficher l'article et ses commentaires", function() {
    browser.addMockModule('httpMocker', function() {
      angular.module('httpMocker', ['ngMockE2E'])
        .run(function($httpBackend) {
          $httpBackend.whenGET('/api/accueil').respond({
            "titre": "Mon titre",
            "contenu": "Mon contenu",
            "commentaires": {
              "commentaire": {
                "auteur": "toto",
                "contenu": "test"
              }
            }
          });
        });
    });

    browser.get("/");
    browser.setLocation("/admin/articleDetails");

    var articleDetail = element(by.css("td")).getText();
    expect(articleDetail).toEqual("La liste est vide");

  });
});
